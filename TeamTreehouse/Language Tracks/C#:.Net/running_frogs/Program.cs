using System;

class Program
{
    static void Main()
    {
        var runningTotal = 0.0;
        
        //	Repeat until the user quits.      
        while (true) 
        {
            
            //	Prompt the user for minutes exercised.
            Console.Write("Enter how many minutes you exercised or type \"quit\" to exit: ");
            
            var entry =  System.Console.ReadLine();
            
            if  (entry.ToLower() =="quit")
            {   
                break;
            }  
               try
               {
                   var minutesExercised = double.Parse(entry);
                    if  (minutesExercised <= 0)
                    {
                        Console.WriteLine(minutesExercised + " is not an acceptable value.");
                        continue;
                            
                        }
                        else if  (minutesExercised <= 10)
                        {
                            Console.WriteLine("Better than nothing, am I right?");    
                        }
                        else if (minutesExercised <= 30)
                        {
                            Console.WriteLine("Way to go, hot stuff!");
                        }            
                        else if ( minutesExercised <= 60)
                        {
                            Console.WriteLine("You must be a Ninja Warrior in training!");
                        }
                        else
                        {
                            Console.WriteLine("Okay, now you're just showing off.");
                        }        
                        //	Add minutes exercised to total.
                        runningTotal += minutesExercised; 
                   }
               catch(FormatException)
               {
                   Console.WriteLine("That is not valid input... Again, I will ask you... ");
                   continue;
               }
                
                    

                

              
                //	Display total minutes exercised to the screen.
                Console.WriteLine("You've entered " + runningTotal + " minutes");             
            
            // Repeat until the user quits.
        }
        
        Console.WriteLine("Goodbye");
        
    }
}
