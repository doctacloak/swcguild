/*$("button").click(function(){
var dieRoll= Math.floor(Math.random() *6 ) +1;
var dieRoll2=Math.floor(Math.random() *6 ) +1;
var dieTotal = dieRoll + dieRoll2;
var message = "You rolled a " + dieRoll + " and a " + dieRoll2 + " for a total of " + dieTotal;
document.getElementById("myMessage").innerHTML = message;
});*/

function hideResults() {
    document.getElementById("results").style.display = "none";
}

function play() {
    var startingBet = document.getElementById("betInput").value;
    var bet = startingBet;
    var dice1;
    var dice2;
    var diceRoll;
    var betsArray = [];

    while (bet > 0) {
        dice1 = Math.floor(Math.random() * 6) +1;
        dice2 = Math.floor(Math.random() * 6) +1;
        diceRoll = dice1 + dice2;
        if(diceRoll != 7) {
            bet -= 1;
        } else {
            bet += 4;
        }

        betsArray.push(bet);
}








    var rollCounter = betsArray.length;
    var highestAmount = Math.max.apply(Math, betsArray);
    var highestPosition = betsArray.indexOf(highestAmount);
    var rollsFromHighest = rollCounter - highestPosition;

    function showResults() {
    document.getElementById("results").style.display = "inline";
    document.getElementById("playButton").innerHTML = "Play Again";
    document.getElementById("startingBet").innerHTML = "$" + startingBet +".00";
    document.getElementById("totalRollsBeforeBroke").innerHTML = rollCounter;
    document.getElementById("highestAmountWon").innerHTML = "$" +     highestAmount + ".00";
    document.getElementById("totalRollsFromHighest").innerHTML = rollsFromHighest;
    };

    showResults();


}
